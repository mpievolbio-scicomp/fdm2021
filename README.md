# FDM Workshop 2021
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2Fmpievolbio-scicomp%2Ffdm2021.git/HEAD?urlpath=lab)

## Jupyter for reproducible science" for the Workshop "Forschungsdatenmanagement" / "Research Data Management" of the Max-Planck-Society

Carsten Fortmann-Grote & Hans Fangohr

Date: May 5 & 6 2021

1. Presentation (25 min + 5 min discussion)  
   The Jupyter notebook format enables seamless coexistence of
   computer program code, documentation, and execution, as well as interactive visualization and discussion of results in one document and provides a user-friendly work environment. 
   We provide an overview of the Jupyter ecosystem of tools and services and discuss how Jupyter enhances reproducibility in data intensive research. The slides are [available as pdf](https://www.desy.de/~fangohr/publications/talks/2021-05-06-fortmanngrote-fangohr-fdm-workshop-Reproducible-science-with-jupyter.pdf).
   
1. Tutorial (30-45 min): We offer a tutorial for interested participants. We first give a basic introduction into the Jupyter notebook format and instruct how to use it in a data analysis setting. We then work through a small data analysis workflow consisting of loading the data into the notebook, data cleanup, and exploratory data analysis with interactive visualisations.

