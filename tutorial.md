# Tutorial
## Intro to jupyter lab
## Your first notebook
### Select a kernel
### Add code
### Add text (markdown)
### Add text (LaTeX)
### Add a picture (drag and drop)
### Add a picture  (from path)
### Add metadata to a cell

## A typical data analysis workflow.
### Load a dataset
### Clean up data
### Summarize and Exploratory data analysis
### Model regression and inference

## A real-world example: modeling a pandemic (maybe apply to more recent data and show why not accurate?)
## Execute the notebook as is
## Make a duplicate and rename
## Regression testing
## Change parameters and document the changes

## Publish on gitlab
## Turn into binder for dissemination

