#! /bin/bash

source /opt/conda/etc/profile.d/conda.sh

pip install --user "arviz==0.11.0" sparqlwrapper
conda install mkl-service -y
